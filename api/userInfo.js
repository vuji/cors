var express = require('express');
var router = express.Router();


/**
 * $.getJSON("https://ee03e075.ngrok.io/api/userInfo",function(d){console.log(d)});
 */

const originRegExp = /eastmoney/g;

router.use(function(req, res, next) {
  
  var origin = req.headers.origin;
  if(originRegExp.test(origin)) {
    res.setHeader('Access-Control-Allow-Origin', origin);
  } else {
    res.removeHeader('Access-Control-Allow-Origin');
  }
  next();
});

var obj = {
      username: '123',
      password: '******'
}


router.get('/', function(req, res, next) {
  res.send(obj);
});

router.get('/edit/:username/:pwd', function(req, res, next) {
  obj.username = username;
  obj.password = pwd;
  res.send(obj);
});

module.exports = router;
