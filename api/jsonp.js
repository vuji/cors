var express = require('express');
var router = express.Router();

var obj = {
    id: "123",
    name: 'vj',
    age: 100
}

/**
 * @SUCCESS : $.get('https://ee03e075.ngrok.io/api/jsonp', function(d){console.log(d);}, 'jsonp');
 * @ERROR : $.get('https://ee03e075.ngrok.io/api/jsonp', function(d){console.log(d);});
 * 
 * 
 *  function jsonpCallback(result) {    
        console.log(result)   
    }    
    var JSONP=document.createElement("script");    
    JSONP.type="text/javascript";    
    JSONP.src="http://127.0.0.1:3000/api/jsonp?callback=jsonpCallback";    
    document.getElementsByTagName("head")[0].appendChild(JSONP);
 * 
 * 
 * 
 */

router.get('/', function(req, res, next) { 
  res.jsonp(obj);
});

module.exports = router;
