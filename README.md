# cors
跨域

## 1. 使用 Access-Control-Allow 解决跨域问题

### Access-Control-Allow-Origin   

该字段是必须的。它的值要么是请求时`Origin`字段的值，要么是一个`*`，表示接受任意域名的请求。  

```
Access-Control-Allow-Origin: <origin> | *
```

origin参数指定一个允许向该服务器提交请求的URI.对于一个不带有credentials的请求,可以指定为'*',表示允许来自所有域的请求.  

### Access-Control-Expose-Headers   

设置浏览器允许访问的服务器的头信息的白名单:  

```
Access-Control-Expose-Headers: X-My-Custom-Header, X-Another-Custom-Header
```

这样, X-My-Custom-Header 和 X-Another-Custom-Header这两个头信息,都可以被浏览器得到.  

### Access-Control-Max-Age

这个头告诉我们这次预请求的结果的有效期是多久,如下:  

```
Access-Control-Max-Age: <delta-seconds>
```

delta-seconds 参数表示,允许这个预请求的参数缓存的秒数,在此期间,不用发出另一条预检请求. 

### Access-Control-Allow-Credentials

告知客户端,当请求的credientials属性是true的时候,响应是否可以被得到.当它作为预请求的响应的一部分时,它用来告知实际的请求是否使用了credentials.注意,简单的GET请求不会预检,所以如果一个请求是为了得到一个带有credentials的资源,而响应里又没有Access-Control-Allow-Credentials头信息,那么说明这个响应被忽略了.

```
Access-Control-Allow-Credentials: true | false
```

### Access-Control-Allow-Methods

指明资源可以被请求的方式有哪些(一个或者多个). 这个响应头信息在客户端发出预检请求的时候会被返回. 上面有相关的例子.

```
Access-Control-Allow-Methods: <method>[, <method>]*
```

### Access-Control-Allow-Headers

也是在响应预检请求的时候使用.用来指明在实际的请求中,可以使用哪些自定义HTTP请求头.比如

```
Access-Control-Allow-Headers: X-PINGOTHER
```

这样在实际的请求里,请求头信息里就可以有这么一条:

```
X-PINGOTHER: pingpong
```

可以有多个自定义HTTP请求头,用逗号分隔.

```
Access-Control-Allow-Headers: <field-name>[, <field-name>]*
```


## 2. 使用`jsonp`解决跨域问题  


## 3. 使用代理  
> 使用中间服务器代理，转发`http`请求

## 4. 使用postmessage  
> 在页面中嵌入`iframe`，使用`iframe`进行`iframe`与主窗口之间进行数据交互


## 5. 使用location.hash + iframe 实现跨域  
> 通过`location.hash`传递数据，监听`iframe`中`src`的变化来实现跨域

## 6. 通过window.name + iframe 实现页面间传值  
> 通过给`window.name`赋值，实现页面间的数据传递

## 7. 使用document.domain + iframe 实现跨域  
> 设置`document.domain`为主域名即可操作`iframe`中的`document`等实现跨域（只有在主域名相同的情况下才能使用该方法）

## 8. 动态创建`script`
> 与`jsonp`类似，直接通过`script`加载数据，并监听`script`

